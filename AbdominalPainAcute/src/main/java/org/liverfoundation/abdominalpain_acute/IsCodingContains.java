package org.liverfoundation.abdominalpain_acute;

import org.hl7.fhir.instance.model.api.IBaseCoding;

import java.util.List;

/**
 *
 * @author Chahal Arora
 */
public class IsCodingContains {
        
    public static Boolean isCodingContains(Object cc, String system, String code){
           
            Boolean result = false;
            IBaseCoding ccd = (IBaseCoding) cc;
            String ccdCode =  ccd.getCode();
            String ccdSystem =  ccd.getSystem();
            if(ccdSystem.equalsIgnoreCase(system) && 
                    ccdCode.equalsIgnoreCase(code)){
                    result = true;
            }
            return result;
    }

}