package org.liverfoundation.urinarysymptomswithorwithoutfever;

import java.time.Period;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.request.CdsRequest ;
import java.util.ArrayList;
import java.util.HashMap;

public class EvalUtil implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    public EvalUtil() {
    }
    
    public static Boolean checkObsLimits(CdsRequest $cdsRequest , String obsType , Integer lowerLimit , Integer higherLimit)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setLowerLimit(lowerLimit);
        fact1.setHigherLimit(higherLimit);
        
        fact1.setCodeSystem("http://opencds.org");
        //fact1.setCodeSystem("http://raxa.com");
        fact1.setConceptName(obsType);
        fact1.setReturnMessage("condition statement message");
        boolean ifCdsResponseExists = fact1.checkIfContainsConcept();
        System.out.println("Result from eval : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    public static Boolean ifObsValueEquals(CdsRequest $cdsRequest , String obsType , String reqdObsValue)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setCodeSystem("http://opencds.org");
        fact1.setConceptName(obsType);
        fact1.setReturnMessage("condition statement message");
        boolean ifCdsResponseExists = fact1.checkIfObsValueEquals(reqdObsValue);
        System.out.println("Result from eval ifObsValueEquals : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    public static Boolean ifObsValuePresent(CdsRequest $cdsRequest , String obsType)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setCodeSystem("http://opencds.org");
        fact1.setConceptName(obsType);
        fact1.setReturnMessage("condition statement message");
        boolean ifCdsResponseExists = fact1.checkIfObsPresent();
        System.out.println("Result from eval ifObsValueEquals : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    
    public static Boolean checkObsExistsInPrefetch(CdsRequest $cdsRequest , String obsType)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setCodeSystem("http://opencds.org");
        fact1.setConceptName(obsType);
        boolean ifObsExistsInPrefetch = fact1.checkIfContainsConceptInPrefetch();
        System.out.println("Result from eval : " + ifObsExistsInPrefetch);
        return ifObsExistsInPrefetch ;
    }
    
    public static Boolean checkObsExistsInPrefetchInGivenPeriod(CdsRequest $cdsRequest , String obsType , String periodString)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setCodeSystem("http://opencds.org");
        fact1.setConceptName(obsType);
        Period requiredPeriod = null;
        try
        {
            requiredPeriod = Period.parse("P"+periodString);
        }
        catch(Exception e)
        {
            System.out.println("Error in parsing PeriodString : " + periodString);
            e.printStackTrace();
        }
        if(requiredPeriod == null)
            return false ;
        boolean ifObsExistsInPrefetch = fact1.checkIfContainsConceptInPrefetchInGivenPeriod(requiredPeriod);
        System.out.println("Result from eval : " + ifObsExistsInPrefetch);
        return ifObsExistsInPrefetch ;
    }
    
    public static Boolean trueFunction()
    {
        System.out.println("RULE EXECUTION STARTED FROM true function");
        return true;
    }
    
    
    public static CdsResponse getCdsResponseWithMessage(String message)
    {
        CdsResponse cdsResponse = new CdsResponse();
        Card card = new Card();
        card.setDetail("<html><body><h1>" + message + "</h1><p>Source - <a href='http://www.nhm.gov.in/images/pdf/guidelines/nrhm-guidelines/stg/Hypertension_QRG.pdf'>STANDARD TREATMENT GUIDELINES for Hypertension Screening, Diagnosis, Assessment, and Management of Primary Hypertension in Adults in India Quick Reference Guide May 2016 Ministry of Health & Family Welfare Government of India, May 2016</a></p></body></html>");
        System.out.println("Adding Card to cdsResponse") ;
        cdsResponse.addCard(card);
        System.out.println(" Now returning cdsResponse , message is : " + message) ;
        return cdsResponse ;
    }
    
    public static Object getCard(String message)
    {
        Card card = new Card();
        card.setDetail("<html><body><h1>" + message + "</h1><p>Source - <a href='http://www.nhm.gov.in/images/pdf/guidelines/nrhm-guidelines/stg/Hypertension_QRG.pdf'>STANDARD TREATMENT GUIDELINES for Hypertension Screening, Diagnosis, Assessment, and Management of Primary Hypertension in Adults in India Quick Reference Guide May 2016 Ministry of Health & Family Welfare Government of India, May 2016</a></p></body></html>");
        return card ;
    }
    
    public static Object getMedicationCard(String medicationName, String doseValue, String doseUnit, String tabletValue, String frequency, String daysValue)
    {
        Card card = new Card();
        String message = "Prescribe "+ medicationName + " " +doseValue+ " " + doseUnit+ ". Take " + tabletValue+ " Tablet " + frequency + " for " + daysValue + " Days." ;
        card.setDetail("<html><body><h1>" + message + "</h1></body></html>");
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setConceptName(medicationName);
        String conceptUuid = fact1.getCodeUuid();
        HashMap medicationDetails = new HashMap();
        medicationDetails.put("conceptUuid", conceptUuid);
        medicationDetails.put("doseValue", doseValue);
        medicationDetails.put("doseUnit", doseUnit);
        medicationDetails.put("frequency", frequency);
        medicationDetails.put("daysValue", daysValue);
        card.setSummary(medicationDetails.toString());
        
        return card;
    }
    
    public static Object getFullMedicationCard(String medicationName , String tabletValue, String frequency, String daysValue)
    {
        //MedicationUtil : static Medication getMedicationFromFullNameAndDosage(String fullName , String daysValue , String frequency , String dosageQuantity)
        Medication medication = MedicationUtil.getMedicationFromFullNameAndDosage(medicationName , daysValue , frequency , tabletValue);
        Card card = new Card();
        String message = "Prescribe "+ medication.getMedicationName() + " " + medication.getStrength() + ". Take " + medication.getDosageQuantity() + " " + medication.getDosageForm() + " " + medication.getFrequency() + " for " + medication.getDaysValue() + " Days." ;
        card.setDetail("<html><body><h1>" + message + "</h1></body></html>");
        HashMap medicationDetails = new HashMap();
        medicationDetails.put("conceptUuid", medication.getConceptUuid());
        //medicationDetails.put("doseValue", medication.getDoseValue());
        //medicationDetails.put("doseUnit", medication.getDoseUnit());
        medicationDetails.put("strength", medication.getStrength());
        medicationDetails.put("frequency", medication.getFrequency());
        medicationDetails.put("daysValue", medication.getDaysValue());
        medicationDetails.put("drugId", medication.getDrugId());
        medicationDetails.put("drugUuid", medication.getDrugUuid());
        medicationDetails.put("dosageForm", medication.getDosageForm());
        medicationDetails.put("dosageQuantity", medication.getDosageQuantity());
        card.setSummary(medicationDetails.toString());
        
        return card;
    }
    
    public static CdsResponse addCardsArrayToCdsResponse(ArrayList contextCards)
    {
        ArrayList<Card> cards = (ArrayList<Card>) contextCards;
        CdsResponse cdsResponse = new CdsResponse();
        for(Card card : cards){
            cdsResponse.addCard(card);
        }
        
        return cdsResponse ;
    }
    
    
    public static Boolean patientAgeCheck(CdsRequest $cdsRequest , String comparator, Integer ageToCompare)
    {
        PatientUtil fact = new PatientUtil();
        boolean ifCdsResponseExists = fact.patientAgeComparator($cdsRequest, comparator,ageToCompare);
        System.out.println("Result from eval patientAgeCheck : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    public static Boolean patientGenderCheck(CdsRequest $cdsRequest , String gender)
    {
        PatientUtil fact = new PatientUtil();
        boolean ifCdsResponseExists = fact.patientGenderComparator($cdsRequest, gender);
        System.out.println("Result from eval patientGenderCheck : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    public static Boolean ifObsInPeriod(CdsRequest $cdsRequest, String obsType,  String comparisonType, String reqdPeriodValue , String reqdPeriodUnit)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        fact1.setCodeSystem("http://opencds.org");
        fact1.setConceptName(obsType);
        fact1.setReturnMessage("condition statement message");
        boolean ifCdsResponseExists = false;
        
        //['years','months','days','hours','minutes','seconds']
        switch(reqdPeriodUnit)
        {
            case "years" :
                         ifCdsResponseExists = fact1.checkIfObsInPeriod("P"+reqdPeriodValue+"Y", comparisonType);
                         break ;
            case "months" :
                         ifCdsResponseExists = fact1.checkIfObsInPeriod("P"+reqdPeriodValue+"M", comparisonType);   
                         break ;
            case "days" :
                         ifCdsResponseExists = fact1.checkIfObsInPeriod("P"+reqdPeriodValue+"D", comparisonType);
                         break ;
            case "hours" :
                         ifCdsResponseExists = fact1.checkIfObsInDuration("PT"+reqdPeriodValue+"H", comparisonType);
                         break ;
            case "minutes" :
                         ifCdsResponseExists = fact1.checkIfObsInDuration("PT"+reqdPeriodValue+"M", comparisonType);
                         break ;
            case "seconds" :
                         ifCdsResponseExists = fact1.checkIfObsInDuration("PT"+reqdPeriodValue+"S", comparisonType);
                         break ;
            default :
                    ifCdsResponseExists = false ;
        }
        
        System.out.println("Result from eval ifObsValueEquals : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }

    public static Boolean checkObsValuePerUnitTime(CdsRequest $cdsRequest , String obsType, String timeUnit , String comparisonType,  Integer obsValue)
    {
        hypertensionUtil fact1 = new hypertensionUtil();
        fact1.setCdsRequest($cdsRequest);
        switch(comparisonType){
                              
                    case "equal to" :
                       fact1.setLowerLimit(obsValue);
                       fact1.setHigherLimit(obsValue);
                      break;

                    case "greater than" :
                      fact1.setLowerLimit(obsValue);
                       fact1.setHigherLimit(null);
                      break;

                    case "less than" :
                      fact1.setLowerLimit(null);
                       fact1.setHigherLimit(obsValue);
                      break;
                                  
         }    
        
        fact1.setCodeSystem("http://opencds.org");
        //fact1.setCodeSystem("http://raxa.com");
        fact1.setConceptName(obsType);
        fact1.setReturnMessage("condition statement message");
        boolean ifCdsResponseExists = fact1.checkIfContainsConceptWithTimeUnitCheck(timeUnit);
        System.out.println("Result from eval : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    
    public static Boolean checkTemperatureLimits(CdsRequest $cdsRequest , String temperatureUnit , Integer lowerLimit , Integer higherLimit)
    {
        boolean ifCdsResponseExists = false ;
        //['Celsius','Fahrenheit']
        if(temperatureUnit.equalsIgnoreCase("Celsius"))
            EvalUtil.checkObsLimits($cdsRequest , "Temperature (C)" , lowerLimit , higherLimit);
        else if (temperatureUnit.equalsIgnoreCase("Fahrenheit"))
            EvalUtil.checkObsLimits($cdsRequest , "Temperature (C)" , toCelsius(lowerLimit) , toCelsius(higherLimit));
        System.out.println("Result from eval : " + ifCdsResponseExists);
        return ifCdsResponseExists ;
    }
    
    public static Integer toCelsius( Integer tempFahrenheit )
    { 
        if(tempFahrenheit == null)
            return null ;
        return (int) ((5.0 / 9.0) * ( tempFahrenheit - 32 ));

    }
}