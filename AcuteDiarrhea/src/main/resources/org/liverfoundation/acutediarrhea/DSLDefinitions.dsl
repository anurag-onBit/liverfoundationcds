[condition][] Patient Record Received= 
               $cdsRequest : CdsRequest()


[condition][] The value of "{observation:ENUM:Observation.observationType}" exists between lower limit {lowerLimit} & higher limit {higherLimit}= 
               eval( EvalUtil.checkObsLimits($cdsRequest , "{observation}" , {lowerLimit} , {higherLimit}) )
               
               
[condition][] The value of "{observation:ENUM:Observation.observationType}" is more than {value}=
               eval( EvalUtil.checkObsLimits($cdsRequest , "{observation}" , {value} , null) )  
               
[condition][] The value of "{observation:ENUM:Observation.observationType}" is less than {value}=
               eval( EvalUtil.checkObsLimits($cdsRequest , "{observation}" , null , {value}) )

[condition][] Has history of "{observation:ENUM:Observation.observationType}"=
               eval( EvalUtil.checkObsExistsInPrefetch($cdsRequest , "{observation}") )  

[condition][] Does not have previous record of "{observation:ENUM:Observation.observationType}"=
               eval( ( ! EvalUtil.checkObsExistsInPrefetch($cdsRequest , "{observation}") ) && ( ! EvalUtil.ifObsValuePresent($cdsRequest , "{observation}")) )                
               
[condition][] Observed Physical Condition "{observation:ENUM:Observation.observationType}" is "{value}"=
               eval( EvalUtil.ifObsValueEquals($cdsRequest , "{observation}" , "{value}"))
               
[condition][] Given Physical Condition "{observation:ENUM:Observation.observationType}"=
               eval( EvalUtil.ifObsValuePresent($cdsRequest , "{observation}"))  

[condition][] Given Physical Condition is not "{observation:ENUM:Observation.observationType}"=
               eval( ! EvalUtil.ifObsValuePresent($cdsRequest , "{observation}"))  
               
[condition][] Patient has "{observation:ENUM:Observation.observationType}" for duration  "{Comparison:ENUM:Comparison.ComparisonOperation}" "{value}" "{TimeUnit:ENUM:TimeUnit.TimeUnitType}"  =
               eval( EvalUtil.ifObsInPeriod($cdsRequest, "{observation}", "{Comparison}","{value}", "{TimeUnit}") )  

[condition][] Patient has "{observation:ENUM:Observation.observationType}" for duration between "{startValue}" "{startTimeUnit:ENUM:TimeUnit.TimeUnitType}" and "{endValue}" "{endTimeUnit:ENUM:TimeUnit.TimeUnitType}"  =
               eval( EvalUtil.ifObsInPeriod($cdsRequest, "{observation}", "greater than equal to","{startValue}", "{startTimeUnit}") && EvalUtil.ifObsInPeriod($cdsRequest, "{observation}", "less than equal to","{endValue}", "{endTimeUnit}") ) 
               
[condition][] Patient's age is "{comparison:ENUM:Comparison.ComparisonOperation}" {value}=
               eval( EvalUtil.patientAgeCheck($cdsRequest, "{comparison}", {value}) )

[condition][] Patient's age is between {valueStart} and {valueEnd}=
               eval( EvalUtil.patientAgeCheck($cdsRequest, "greater than equal to", {valueStart}) && EvalUtil.patientAgeCheck($cdsRequest, "less than equal to", {valueEnd}) )

[condition][] Patient's gender is "{gender:ENUM:Gender.GenderType}"=
               eval( EvalUtil.patientGenderCheck($cdsRequest, "{gender}") )     
               
[condition][] Recorded "{observation:ENUM:Observation.observationType}" per "{TimeUnit:ENUM:TimeUnit.TimeUnitTypeSingular}" is "{Comparison:ENUM:Comparison.ComparisonOperation}" "{value}"  =
               eval( EvalUtil.checkObsValuePerUnitTime($cdsRequest , "{observation}", "{TimeUnit}" , "{Comparison}",  {value}) )  
               
[condition][] Found history of "{observation:ENUM:Observation.observationType}" in "{periodString}" Eg 3Y5M6D =
               eval( EvalUtil.checkObsExistsInPrefetchInGivenPeriod($cdsRequest , "{observation}" , "{periodString}") )                

[consequence][] Send back instruction "{instruction}" =
                
                System.out.println("Getting instruction");
                System.out.println(EvalUtil.getCard("{instruction}"));
                cardsArray.add(EvalUtil.getCard("{instruction}"));
                System.out.println(cardsArray);
                System.out.println(cardsArray.size());
                ArrayList cards = new ArrayList();
                System.out.println(cards.size());
                cards.addAll(cardsArray);
                System.out.println(cards.size());
                namedObjects.put("CdsResponse", EvalUtil.addCardsArrayToCdsResponse(cards));
                System.out.println(namedObjects);
                System.out.println("Named objects size" +namedObjects.size());
                
[consequence][] Prescribe "{medication:ENUM:Medication.FullName}" Take  '{tabletValue}'  tablet  "{frequency:ENUM:Frequency.FrequencyType}"  for  "{daysValue}" Days.=

                System.out.println("Getting medication");
                cardsArray.add(EvalUtil.getFullMedicationCard("{medication}", "{tabletValue}" , "{frequency}", "{daysValue}"));
                System.out.println(cardsArray);
                System.out.println(cardsArray.size());
                ArrayList cards = new ArrayList();
                System.out.println(cards.size());
                cards.addAll(cardsArray);
                System.out.println(cards.size());
                namedObjects.put("CdsResponse", EvalUtil.addCardsArrayToCdsResponse(cards));
                System.out.println(namedObjects);
                System.out.println("Named objects size" +namedObjects.size());                
