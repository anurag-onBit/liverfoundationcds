package org.liverfoundation.respiratorydistress;

import java.io.FileInputStream;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import org.hl7.fhir.instance.model.Resource;
import org.hl7.fhir.instance.model.Observation;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.DateTimeType;
import org.hl7.fhir.instance.model.ResourceType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;

public class hypertensionUtil implements java.io.Serializable
{
    
    static {
        System.out.println("conceptJson static block started");
        try{
        String conceptJsonStr = readFile() ;
        JSONParser parser = new JSONParser();
        Object medicationJsonObj = parser.parse(conceptJsonStr);
        conceptJson = (JSONObject) medicationJsonObj;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        } 
    }
    
   static JSONObject conceptJson ;

   static final long serialVersionUID = 1L;

   //@org.kie.api.definition.type.Label("Cds Request")
   private org.opencds.hooks.model.request.CdsRequest cdsRequest;
   //@org.kie.api.definition.type.Label("Cds Response")
   private org.opencds.hooks.model.response.CdsResponse cdsResponse;
   //@org.kie.api.definition.type.Label("concept code Id")
   private java.lang.String codeId;
   //@org.kie.api.definition.type.Label("code system value")
   private java.lang.String codeSystem;
   //@org.kie.api.definition.type.Label("Higher Limit")
   private java.lang.Integer higherLimit;
   //@org.kie.api.definition.type.Label("Lower Limit")
   private java.lang.Integer lowerLimit;
   //@org.kie.api.definition.type.Label("Return Message")
   private java.lang.String returnMessage;

   //@org.kie.api.definition.type.Label("Name of the concept")
   private java.lang.String conceptName;

   private java.lang.String codeUuid;

   public hypertensionUtil()
   {
   }

   public org.opencds.hooks.model.request.CdsRequest getCdsRequest()
   {
      return this.cdsRequest;
   }

   public void setCdsRequest(
         org.opencds.hooks.model.request.CdsRequest cdsRequest)
   {
      this.cdsRequest = cdsRequest;
   }

   public org.opencds.hooks.model.response.CdsResponse getCdsResponse()
   {
      return this.cdsResponse;
   }

   public void setCdsResponse(
         org.opencds.hooks.model.response.CdsResponse cdsResponse)
   {
      this.cdsResponse = cdsResponse;
   }

   public java.lang.String getCodeId()
   {
      JSONObject conceptDetailsJson = (JSONObject) conceptJson.get(this.conceptName);
      this.codeId= (conceptDetailsJson.get("ConceptId").toString());
      return this.codeId;
   }

   public void setCodeId(java.lang.String codeId)
   {
      this.codeId = codeId;
   }

   public java.lang.String getCodeSystem()
   {
      return this.codeSystem;
   }

   public void setCodeSystem(java.lang.String codeSystem)
   {
      this.codeSystem = codeSystem;
   }

   public java.lang.Integer getHigherLimit()
   {
      return this.higherLimit;
   }

   public void setHigherLimit(java.lang.Integer higherLimit)
   {
      this.higherLimit = higherLimit;
   }

   public java.lang.Integer getLowerLimit()
   {
      return this.lowerLimit;
   }

   public void setLowerLimit(java.lang.Integer lowerLimit)
   {
      this.lowerLimit = lowerLimit;
   }

   public java.lang.String getReturnMessage()
   {
      return this.returnMessage;
   }

   public void setReturnMessage(java.lang.String returnMessage)
   {
      this.returnMessage = returnMessage;
   }
   
   public boolean checkIfObsPresent()
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            {
                try{
                       if (contextEntry instanceof Observation)
                       {
                          Observation obs = (Observation) contextEntry;
                          if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                          {
                                 return true ;
                          }
                       }
                  }
                  catch(Exception e)
                  {
                      e.printStackTrace();
                  }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }
   
   public boolean checkIfObsValueEquals(String reqdObsValue)
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            { 
               try
               {
                   if (contextEntry instanceof Observation)
                   {
                      Observation obs = (Observation) contextEntry;
                      if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                      {
                         
                            String valueStringType = obs.getComponent().get(0).getValueStringType().asStringValue();
    
                            if (valueStringType.equalsIgnoreCase(reqdObsValue))
                            {
                               return true;
                            }
    
    
                      }
                   }
              }
              catch(Exception e)
              {
                  e.printStackTrace();
              }
               
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }

   public boolean checkIfContainsConcept()
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            {
               try
               {
                       if (contextEntry instanceof Observation)
                       {
                          Observation obs = (Observation) contextEntry;
                          if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                          {
                             if (minimumValue != null && maximumValue != null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() >= minimumValue && obs.getComponent().get(0).getValueQuantity().getValue().intValue() <= maximumValue)
                                {
        
                                   return true;
        
                                }
        
                             }
                             else if (minimumValue == null && maximumValue != null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() <= maximumValue)
                                {
                                   return true;
        
                                }
        
                             }
                             else if (minimumValue != null && maximumValue == null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() >= minimumValue)
                                {
                                   return true;
        
                                }
        
                             }
        
                          }
                       }
               }
               catch(Exception e)
               {
                   System.out.println("Problem with observation .");
                   e.printStackTrace();
               }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }
   
   public boolean checkIfContainsConceptWithTimeUnitCheck(String timeUnit)
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            {
               try
               {
                       if (contextEntry instanceof Observation)
                       {
                          Observation obs = (Observation) contextEntry;
                          if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                          {
                             if (minimumValue != null && maximumValue != null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() >= minimumValue && obs.getComponent().get(0).getValueQuantity().getValue().intValue() <= maximumValue)
                                {
        
                                   return true && obs.getComponent().get(0).getValueQuantity().getUnit().equals("per " + timeUnit);
        
                                }
        
                             }
                             else if (minimumValue == null && maximumValue != null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() <= maximumValue)
                                {
                                   return true && obs.getComponent().get(0).getValueQuantity().getUnit().equals("per " + timeUnit) ;
        
                                }
        
                             }
                             else if (minimumValue != null && maximumValue == null)
                             {
        
                                if (obs.getComponent().get(0).getValueQuantity().getValue().intValue() >= minimumValue)
                                {
                                   return true && obs.getComponent().get(0).getValueQuantity().getUnit().equals("per " + timeUnit);
        
                                }
        
                             }
        
                          }
                       }
               }
               catch(Exception e)
               {
                   System.out.println("Problem with observation .");
                   e.printStackTrace();
               }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }

   public boolean checkIfContainsConceptInPrefetch()
   {

      if (this.cdsRequest != null)
      {
         try
         {

            Map<String, BundleEntryComponent> prefetch = this.cdsRequest.getPrefetch();

            Object entryObject = prefetch.get("entry");
            List<BundleEntryComponent> entryList = (List<BundleEntryComponent>) entryObject;
            for (BundleEntryComponent entry : entryList)
            {
                try
                {

                       ResourceType resourceType = entry.getResource().getResourceType();
                       if (resourceType.equals(ResourceType.Observation))
                       {
                          Resource resource = entry.getResource();
                          Observation obs = (Observation) resource;
                          CodeableConcept code = obs.getCode();
                          List<Coding> coding = code.getCoding();
                          String openmrsCode = "NoOpenmrsCode";
                          for (Coding eachCoding : coding)
                          {
                             if (eachCoding.getSystem().equals(this.codeSystem))
                             {
                                System.out.println("openmrs code present");
                                openmrsCode = eachCoding.getCode();
                                eachCoding.setCode(openmrsCode);
                                if (IsCodingContains.isCodingContains(eachCoding, this.codeSystem, this.getCodeUuid()))
                                {
                                   return true;
                                }
                             }
                          }
                          System.out.println("openmrs code is :" + openmrsCode);
        
                       }
              } 
              catch(Exception e)
              {
                  e.printStackTrace();
              }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }
   
   public boolean checkIfContainsConceptInPrefetchInGivenPeriod(Period requiredPeriod)
   {

      if (this.cdsRequest != null)
      {
         try
         {

            Map<String, BundleEntryComponent> prefetch = this.cdsRequest.getPrefetch();

            Object entryObject = prefetch.get("entry");
            List<BundleEntryComponent> entryList = (List<BundleEntryComponent>) entryObject;
            for (BundleEntryComponent entry : entryList)
            {
                try
                {

                       ResourceType resourceType = entry.getResource().getResourceType();
                       if (resourceType.equals(ResourceType.Observation))
                       {
                          Resource resource = entry.getResource();
                          Observation obs = (Observation) resource;
                          CodeableConcept code = obs.getCode();
                          List<Coding> coding = code.getCoding();
                          String openmrsCode = "NoOpenmrsCode";
                          for (Coding eachCoding : coding)
                          {
                             if (eachCoding.getSystem().equals(this.codeSystem))
                             {
                                System.out.println("openmrs code present");
                                openmrsCode = eachCoding.getCode();
                                eachCoding.setCode(openmrsCode);
                                if (IsCodingContains.isCodingContains(eachCoding, this.codeSystem, this.getCodeUuid()))
                                {
                                    DateTimeType effectiveDateTimeType = obs.getEffectiveDateTimeType();
                                    LocalDateTime obsLocalDateTime = DateTimeUtil.toLocalDateTime(effectiveDateTimeType.toCalendar());
                                    if(DateTimeUtil.isDateWithinPeriod(requiredPeriod, obsLocalDateTime))
                                            return true;
                                }
                             }
                          }
                          System.out.println("openmrs code is :" + openmrsCode);
        
                       }
              } 
              catch(Exception e)
              {
                  e.printStackTrace();
              }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }

   public java.lang.String getConceptName()
   {
      return this.conceptName;
   }

   public void setConceptName(java.lang.String conceptName)
   {
      this.conceptName = conceptName;
   }

   public java.lang.String getCodeUuid()
   {
      JSONObject conceptDetailsJson = (JSONObject) conceptJson.get(this.conceptName);
      this.codeUuid = (conceptDetailsJson.get("ConceptId").toString());
      return this.codeUuid;
    }

   public void setCodeUuid(java.lang.String codeUuid)
   {
      this.codeUuid = codeUuid;
   }

   public hypertensionUtil(org.opencds.hooks.model.request.CdsRequest cdsRequest,
         org.opencds.hooks.model.response.CdsResponse cdsResponse,
         java.lang.String codeId, java.lang.String codeSystem,
         java.lang.Integer higherLimit, java.lang.Integer lowerLimit,
         java.lang.String returnMessage, java.lang.String conceptName,
         java.lang.String codeUuid)
   {
      this.cdsRequest = cdsRequest;
      this.cdsResponse = cdsResponse;
      this.codeId = codeId;
      this.codeSystem = codeSystem;
      this.higherLimit = higherLimit;
      this.lowerLimit = lowerLimit;
      this.returnMessage = returnMessage;
      this.conceptName = conceptName;
      this.codeUuid = codeUuid;
   }
   
       public static String readFile()
    {
        FileInputStream inputStream = null;
        String everything = "DEFAULT_EVRYTHING";
        try {
            inputStream = new FileInputStream("/root/.opencds/opencds-knowledge-repository-data/resources_v1.3/supportingData/conceptJson.txt");
            everything = IOUtils.toString(inputStream);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return everything ;
    }
       
       
    //temporal work
       
    public boolean checkIfObsInPeriod(String reqdPeriodValue, String comparisonType)
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            { 
               try
               {
                   if (contextEntry instanceof Observation)
                   {
                      Observation obs = (Observation) contextEntry;
                      if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                      {
                         
                            //String valueStringType = obs.getComponent().get(2).getValueStringType().asStringValue();
                            org.hl7.fhir.instance.model.Period valuePeriod = obs.getComponent().get(1).getValuePeriod();
                            //if(valueStringType.startsWith("PERIOD_")) 
                            {
                                //org.hl7.fhir.instance.model.Period x = new org.hl7.fhir.instance.model.Period();
                                Calendar startCalendar = valuePeriod.getStartElement().toCalendar();
                                Calendar endCalendar = valuePeriod.getEndElement().toCalendar();
                                //Period obsPeriod = Period.parse(valueStringType.substring(7));
                                Period obsPeriod = Period.between(DateTimeUtil.toLocalDateTime(startCalendar).toLocalDate(),DateTimeUtil.toLocalDateTime(endCalendar).toLocalDate());
                                Period reqPeriod = Period.parse(reqdPeriodValue);
                                
                                boolean periodComparisonSuccess = false ;
                                
                                switch(comparisonType){
                              
                                case "equal to" :
                                  periodComparisonSuccess = DateTimeUtil.comparePeriodsByDays(obsPeriod, reqPeriod) == 0 ;
                                  break;
                                
                                case "greater than" :
                                  periodComparisonSuccess = DateTimeUtil.comparePeriodsByDays(obsPeriod, reqPeriod) > 0 ;
                                  break;
                                
                                case "less than" :
                                  periodComparisonSuccess = DateTimeUtil.comparePeriodsByDays(obsPeriod, reqPeriod) < 0 ;
                                  break;
                                  
                                case "greater than equal to" :
                                  periodComparisonSuccess = DateTimeUtil.comparePeriodsByDays(obsPeriod, reqPeriod) >= 0 ;
                                  break;
                                    
                                case "less than equal to" :
                                  periodComparisonSuccess = DateTimeUtil.comparePeriodsByDays(obsPeriod, reqPeriod) <= 0;
                                  break;
                                    
                                default :
                                  System.out.println("Invalid comparison type : " + comparisonType);
                                  break;
                              
                                }
    
                                   return periodComparisonSuccess;
                                
                            }
                            
    
    
                      }
                   }
              }
              catch(Exception e)
              {
                  e.printStackTrace();
              }
               
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   }   
    
    
   public boolean checkIfObsInDuration(String reqdPeriodValue, String comparisonType)
   {
      Integer minimumValue = this.lowerLimit;
      Integer maximumValue = this.higherLimit;
      String message = this.returnMessage != null ? this.returnMessage : "";

      if (this.cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = this.cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            { 
               try
               {
                   if (contextEntry instanceof Observation)
                   {
                      Observation obs = (Observation) contextEntry;
                      if (IsCodingContains.isCodingContains(obs.getCode().getCoding().get(0), this.codeSystem, this.getCodeId()))
                      {
                         
                            //String valueStringType = obs.getComponent().get(2).getValueStringType().asStringValue();
                            org.hl7.fhir.instance.model.Period valuePeriod = obs.getComponent().get(1).getValuePeriod();
                            //if(valueStringType.startsWith("PERIOD_")) 
                            {
                                //org.hl7.fhir.instance.model.Period x = new org.hl7.fhir.instance.model.Period();
                                Calendar startCalendar = valuePeriod.getStartElement().toCalendar();
                                Calendar endCalendar = valuePeriod.getEndElement().toCalendar();
                                //Period obsPeriod = Period.parse(valueStringType.substring(7));
                                Duration obsDuration = Duration.between( startCalendar.toInstant(), startCalendar.toInstant()) ;
                                Duration reqDuration = Duration.parse(reqdPeriodValue);
                                //Period obsPeriod = Period.between(DateTimeUtil.toLocalDateTime(startCalendar).toLocalDate(),DateTimeUtil.toLocalDateTime(endCalendar).toLocalDate());
                                //Period reqPeriod = Period.parse(reqdPeriodValue);
                                int compareTo = obsDuration.compareTo(reqDuration);
                                
                                boolean periodComparisonSuccess = false ;
                                
                                switch(comparisonType){
                              
                                case "equal to" :
                                  periodComparisonSuccess = compareTo == 0 ;
                                  break;
                                
                                case "greater than" :
                                  periodComparisonSuccess = compareTo > 0 ;
                                  break;
                                
                                case "less than" :
                                  periodComparisonSuccess = compareTo < 0 ;
                                  break;
                                  
                                case "greater than equal to" :
                                  periodComparisonSuccess = compareTo >= 0 ;
                                  break;
                                    
                                case "less than equal to" :
                                  periodComparisonSuccess = compareTo <= 0;
                                  break;
                                    
                                default :
                                  System.out.println("Invalid comparison type : " + comparisonType);
                                  break;
                              
                                }
    
                                   return periodComparisonSuccess;
                                
                            }
                            
    
    
                      }
                   }
              }
              catch(Exception e)
              {
                  e.printStackTrace();
              }
               
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return false;
   } 

}