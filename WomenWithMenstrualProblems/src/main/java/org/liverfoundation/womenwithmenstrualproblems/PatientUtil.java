package org.liverfoundation.womenwithmenstrualproblems;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.hl7.fhir.instance.model.Enumerations;
import org.hl7.fhir.instance.model.Observation;
import org.hl7.fhir.instance.model.Patient;
import org.hl7.fhir.instance.model.Resource;
import org.opencds.hooks.model.request.CdsRequest;

/**
 *
 * @author carora
 */
public class PatientUtil implements java.io.Serializable {
    
    static final long serialVersionUID = 1L;
    
    
   public PatientUtil() {
   }

    
//    public boolean isPatientAgeGreaterThanMentionedAge(Patient patient, int ageToCompare){
//           boolean isAgeGreaterThan = false;
//           Date birthDate = patient.getBirthDate();
//           
//           Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
//           
//           if(age.getYears() > ageToCompare) {
//               isAgeGreaterThan = true;
//           }
//           
//           return isAgeGreaterThan;
//    }
//    
//    public boolean isPatientAgeGreaterThanOrEqualToMentionedAge(Patient patient, int ageToCompare){
//           boolean isAgeGreaterThan = false;
//           Date birthDate = patient.getBirthDate();
//           
//           Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
//           
//           if(age.getYears() >= ageToCompare) {
//               isAgeGreaterThan = true;
//           }
//           
//           return isAgeGreaterThan;
//    }
//    
//    
//    public boolean isPatientAgeLessThanMentionedAge(Patient patient, int ageToCompare){
//           boolean isAgeGreaterThan = false;
//           Date birthDate = patient.getBirthDate();
//           
//           Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
//           
//           if(age.getYears() < ageToCompare) {
//               isAgeGreaterThan = true;
//           }
//           
//           return isAgeGreaterThan;
//    }
//    
//    public boolean isPatientAgeLessThanOrEqualToMentionedAge(Patient patient, int ageToCompare){
//           boolean isAgeGreaterThan = false;
//           Date birthDate = patient.getBirthDate();
//           
//           Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
//           
//           if(age.getYears() <= ageToCompare) {
//               isAgeGreaterThan = true;
//           }
//           
//           return isAgeGreaterThan;
//    }
//    
//    public boolean isPatientAgeEqualToMentionedAge(Patient patient, int ageToCompare){
//           boolean isAgeGreaterThan = false;
//           Date birthDate = patient.getBirthDate();
//           
//           Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
//           
//           if(age.getYears() == ageToCompare) {
//               isAgeGreaterThan = true;
//           }
//           
//           return isAgeGreaterThan;
//    }
    
    
    
    public boolean patientAgeComparator(CdsRequest cdsRequest, String comparisonType, Integer ageToCompare)
    {
      boolean ageComparisonSuccess = false; 
      if (cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            {
                try{
                       if (contextEntry instanceof Patient)
                       {
                          Patient patient = (Patient) contextEntry;
                          Date birthDate = patient.getBirthDate();
                          Period age = Period.between(LocalDateTime.ofInstant(birthDate.toInstant(), ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
                          
                          switch(comparisonType){
                              
                                case "equal to" :
                                  ageComparisonSuccess = age.getYears() == ageToCompare;
                                  break;
                                
                                case "greater than" :
                                  ageComparisonSuccess = age.getYears() > ageToCompare;
                                  break;
                                
                                case "less than" :
                                  ageComparisonSuccess = age.getYears() < ageToCompare;
                                  break;
                                  
                                case "greater than equal to" :
                                  ageComparisonSuccess = age.getYears() >= ageToCompare;
                                  break;
                                    
                                case "less than equal to" :
                                  ageComparisonSuccess = age.getYears() <= ageToCompare;
                                  break;
                                    
                                default :
                                  System.out.println("Invalid comparison type : " + comparisonType);
                                  break;
                              
                          }
                          
                       }
                  }
                  catch(Exception e)
                  {
                      e.printStackTrace();
                  }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return ageComparisonSuccess;
   }
    
    
    
   public boolean patientGenderComparator(CdsRequest cdsRequest, String gender)
    {
      boolean genderComparisonSuccess = false; 
      if (cdsRequest != null)
      {
         try
         {
            List<Resource> cdsRequestContext = cdsRequest.getContext();

            for (Resource contextEntry : cdsRequestContext)
            {
                try{
                       if (contextEntry instanceof Patient)
                       {
                          Patient patient = (Patient) contextEntry;
                          genderComparisonSuccess = patient.getGender().getDisplay().equalsIgnoreCase(gender);

                          
                       }
                  }
                  catch(Exception e)
                  {
                      e.printStackTrace();
                  }
            }

         }

         catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Cds Request is not null");

      }
      else
      {
         System.out.println("Cds Request is null");

      }

      return genderComparisonSuccess;
   }

    
}
